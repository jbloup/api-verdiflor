<?php

namespace App\Http\Controllers;

use App\Models\Devis;
use PDO;


class DevisController extends Controller
{

    /**
     * @param Devis $devis
     * @return array|false|string|string[]|null
     */
    public function devis(Devis $devis)
    {
        //ini_set('memory_limit', '2048M');

        $pdo = new PDO('odbc:CODIAL');
        $statement = $pdo->prepare($devis->devisQuery);
        $statement->execute();

        $statement2 = $pdo->prepare($devis->ligneDevisQuery);
        $statement2->execute();

        $all_devis = mb_convert_encoding($statement->fetchAll(), 'UTF-8');
        $all_line_devis = mb_convert_encoding($statement2->fetchAll(), 'UTF-8');

        for ($i = 0; $i < count($all_devis); $i++){
            $one_devis = $all_devis[$i];
            $one_line_devis = $all_line_devis[$i];

            if($one_devis['id'] == $one_line_devis['docNumber']) {
                $one_devis['lignes'] = $all_line_devis[$i];
            }

//            for ($k = 0; $k < count($one_devis); $k++){
//                $strip_one_devis[$k] = strip_tags($one_devis[$k]);
//                $strip_one_line_devis[$k] = strip_tags($one_line_devis[$k]);
//                }

            $tab[$i] = $one_devis;

        }

        return $tab;

        // Eloquent sur HFSQL
        //$devis = DB::connection('odbc-codial')->table('devis')->where('code', 1)->get();
        //return $devis;
    }
    /**
     * Display a listing of the resource.
     *
     * @return array|false|string|string[]
     */
    public function ligneDevis(Devis $devis)
    {
        $pdo = new PDO('odbc:CODIAL');
        $statement = $pdo->prepare($devis->ligneDevisQuery);
        $statement->execute();

        return mb_convert_encoding($statement->fetchAll(), 'UTF-8', 'UTF-8');
    }
}
