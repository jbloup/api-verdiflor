<?php

namespace App\Http\Controllers;

use App\Models\Client;
use PDO;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array|false|string|string[]
     */
    public function clients(Client $client)
    {
        $pdo = new PDO('odbc:CODIAL');
        $statement = $pdo->prepare($client->clientQuery);
        $statement->execute();

        return mb_convert_encoding($statement->fetchAll(), 'UTF-8', 'UTF-8');
    }

    /**
     * Display a listing of the resource.
     *
     * @return array|false|string|string[]
     */
    public function address(Client $client)
    {
        $pdo = new PDO('odbc:CODIAL');
        $statement = $pdo->prepare($client->addressQuery);
        $statement->execute();

        return mb_convert_encoding($statement->fetchAll(), 'UTF-8', 'UTF-8');
    }
    /**
     * Display a listing of the resource.
     *
     * @return array|false|string|string[]
     */
    public function address_client(Client $client)
    {
        $pdo = new PDO('odbc:CODIAL');
        $statement = $pdo->prepare($client->clientAddressQuery);
        $statement->execute();

        return mb_convert_encoding($statement->fetchAll(), 'UTF-8', 'UTF-8');
    }
}
