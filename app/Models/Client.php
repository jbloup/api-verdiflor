<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    public string $clientQuery = "select client.code as id, client.nom as name, clienti.IDCLIENTI as addres
                                    from client join clienti on client.code = clienti.code";

    public string $addressQuery = "select clienti.IDCLIENTI as id, clienti.code as clientCode, clienti.nom as name, clienti.adresse1 as street1, clienti.adresse2 as street2, clienti.cop as zip, clienti.ville as city, ifnull(FA4_PARAM_PAYS.NOM,'') as country, ifnull(FA4_TIERS_CONTACT.TEL_CONTACT,'') as phoneCompany, ifnull(FA4_TIERS_CONTACT.PORT_CONTACT,'') as phone, ifnull(FA4_TIERS_CONTACT.EMAIL_CONTACT,'') as email
                                            from clienti LEFT join FA4_PARAM_PAYS on clienti.CODE_PAYS_LIVR = FA4_PARAM_PAYS.code
                                            left join FA4_TIERS_CONTACT on FA4_TIERS_CONTACT.ID_ADRESSE = clienti.IDCLIENTI and FA4_TIERS_CONTACT.INT_PRINCIPALE = 1";
    public string $clientAddressQuery = "select clienti.code as clientCode, client.nom as clientName, clienti.IDCLIENTI as id, clienti.nom as nameAddresse, clienti.adresse1 as street1, clienti.adresse2 as street2, clienti.cop as zip, clienti.ville as city, ifnull(FA4_PARAM_PAYS.NOM,'') as country, ifnull(FA4_TIERS_CONTACT.TEL_CONTACT,'') as phoneCompany, ifnull(FA4_TIERS_CONTACT.PORT_CONTACT,'') as phone, ifnull(FA4_TIERS_CONTACT.EMAIL_CONTACT,'') as email
                                            from clienti join client on client.code = clienti.code
                                            LEFT join FA4_PARAM_PAYS on clienti.CODE_PAYS_LIVR = FA4_PARAM_PAYS.code
                                            left join FA4_TIERS_CONTACT on FA4_TIERS_CONTACT.ID_ADRESSE = clienti.IDCLIENTI and FA4_TIERS_CONTACT.INT_PRINCIPALE = 1";
}
