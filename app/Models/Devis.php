<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Statement;

class Devis extends Model
{
    use HasFactory;

    public string $devisQuery = "select distinct devis.D0CLEUNIK as id, devis.numerod as docNumber, devis.SUJET as subject, devis.REF_CLIENT as clientRef, FA4_ETABLISSEMENT.NOM as etablishmentStock, devis.DATEHEURE_CREA as createdAt, devis.DATE as date,
devis.toperat as affectation, devis.CODE as client, concat(meca.NOM,' ',meca.PRENOM) as commercial,
case WHEN devis.ETATD = 1 THEN 'En attente' WHEN devis.ETATD = 2 THEN 'Sans suite' WHEN devis.ETATD = 3 THEN 'Refusé' WHEN devis.ETATD = 4 THEN 'Accepté' END as state,
case WHEN devis.INT_DEVIS_TYPE = 1 THEN 'Type' WHEN devis.INTIMPD = 1 THEN 'Printed' WHEN devis.INT_INTRACOM = 1 THEN 'VAT' WHEN devis.INT_HTTTC = 1 THEN 'TTC' else '' END as status,
COD_SYNTHESE_DOC.TOTAL_HT_FINAL as subTotal, COD_SYNTHESE_DOC.TOTAL_TTC_FINAL as total, COD_SYNTHESE_DOC.TOTAL_TTC_FINAL-COD_SYNTHESE_DOC.TOTAL_HT_FINAL as vatTotal,
round(((COD_SYNTHESE_DOC.TOTAL_TTC_FINAL/COD_SYNTHESE_DOC.TOTAL_HT_FINAL)-1)*100,2) as vatPercentage
from devis join COD_SYNTHESE_DOC on devis.NUMEROD = COD_SYNTHESE_DOC.NUM_DOC and COD_SYNTHESE_DOC.TYPE_DOC = 6
join FA4_ETABLISSEMENT on FA4_ETABLISSEMENT.CODE_ETABLISSEMENT = devis.ETAB
join MECA on MECA.CODEREP = devis.CODEREP";

    public string $ligneDevisQuery = "select devis.D0CLEUNIK as docNumber, lignedev.NUMLIGNE as numero, lignedev.REFERENCE as reference, lignedev.TYPE_LIGNE as type, lignedev.DESIGN as designation, lignedev.QTEU as qteu, lignedev.qte as qteOeuvre, lignedev.UNITE as unit,
lignedev.PRIX_ACHAT as pAchat, lignedev.PRIXU_HT_PUBLIC/lignedev.PRIX_ACHAT as coefPV, lignedev.PRIXU_HT_PUBLIC as pPbHt, lignedev.REMISE as remise,
lignedev.MONTANTU as prixUnitHt, lignedev.MONTANTT as montNetHt, lignedev.MONTANTT as totalHt, ifnull(FA4_TAG_LGN_DOC.DESCRIPTION,'') as tag
from lignedev join devis on devis.D0CLEUNIK = lignedev.D0CLEUNIK
left join FA4_TAG_LGN_DOC on FA4_TAG_LGN_DOC.IDFA4_TAG_LIGN_DOC = lignedev.IDFA4_TAG_LIGN_DOC and FA4_TAG_LGN_DOC.TYP_DOC = 6";

}
