<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\DevisController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('devis', [DevisController::class, 'devis']);
Route::get('ligne-devis', [DevisController::class, 'ligneDevis']);

Route::get('clients', [ClientController::class, 'clients']);
Route::get('address', [ClientController::class, 'address']);
Route::get('address-clients', [ClientController::class, 'address_clients']);
